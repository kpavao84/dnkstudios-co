var gulp = require('gulp');
var bower = require('gulp-bower');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');

var config = {
    sassDir: './sass/',
    bowerDir: './bower_components',
}

gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(config.bowerDir))
});

gulp.task('sass', function() {
    gulp.src(config.sassDir + '/**/*.scss')
        .pipe(sass({
            includePaths: [config.bowerDir + '/bootstrap-sass-official/assets/stylesheets'],
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('fonts', function() {
    return gulp.src(config.bowerDir + '/bootstrap-sass-official/assets/fonts/**/*')
        .pipe(gulp.dest('./fonts'));
});

//gulp.task('minify-css', function() {
//  return gulp.src('css/*.css')
//    .pipe(minifyCss({compatibility: 'ie8'}))
//    .pipe(gulp.dest('./css'));
//});

gulp.task('watch', function() {
    gulp.watch(config.sassDir + '/**/*.scss', ['sass']);
});

gulp.task('default', ['bower', 'sass', 'fonts']);

